﻿/*
==================================================================================================
                            SERVICES ~ Définition des services de l'application

-- Les services sont accesibles de partout et permettent de gérer les données utilisées par l'application
==================================================================================================
*/

angular.module("GalaxyMedoc.services", [])


/*
==================================================================================================
                            SERVICES ~ Définition du service UserSvc

-- Ce service gère toutes les données relatives à l'utilisateur connecté (infos, paramètres, login, logout...)
==================================================================================================
*/

.service("UsersSvc", function ($state) {
    var self = this;
    self.user = JSON.parse(window.localStorage.getItem("user"));
    self.settings = JSON.parse(window.localStorage.getItem("settings"));

    self.updateUser = function (user, store) {
        self.user = user;

        if (store)
            window.localStorage.setItem("user", JSON.stringify(self.user));
    };

    self.updateSettings = function (settings, store) {
        self.settings = settings;

        if(store)
            window.localStorage.setItem("settings", JSON.stringify(self.settings));
    };

    self.checkConnected = function () {
        if (!self.user) {
            $state.go("login");
            return false;
        }

        return true;
    };
})


/*
==================================================================================================
                            SERVICES ~ Définition du service VisiteursSvc

-- Ce service gère toutes les données relatives aux visiteurs
==================================================================================================
*/

.service("VisiteursSvc", function ($http, UsersSvc) {
    var self = this;
    self.visiteurs = [];

    self.loadVisiteurs = $http.get("http://" + UsersSvc.settings.server + "/api/visiteur").success(function (response) {
        self.visiteurs = response;
    });

    self.get = function (visiteurId) {
        for (var i = 0; i < self.visiteurs.length; i++) {
            if (self.visiteurs[i].Id == parseInt(visiteurId))
                return self.visiteurs[i];
        }

        return null;
    };
})


/*
==================================================================================================
                            SERVICES ~ Définition du service MedicamentsSvc

-- Ce service gère toutes les données relatives aux médicaments
==================================================================================================
*/

.service("MedicamentsSvc", function ($http, UsersSvc) {
    var self = this;
    self.medicaments = [];

    self.loadMedicaments = $http.get("http://" + UsersSvc.settings.server + "/api/medicament").success(function (response) {
        self.medicaments = response;
    });

    self.get = function (medicamentId) {
        for (var i = 0; i < self.medicaments.length; i++) {
            if (self.medicaments[i].Id == parseInt(medicamentId))
                return self.medicaments[i];
        }

        return null;
    };
})


/*
==================================================================================================
                            SERVICES ~ Définition du service CommandesSvc

-- Ce service gère toutes les données relatives aux commandes
==================================================================================================
*/

.service("CommandesSvc", function ($http, UsersSvc, PraticiensSvc, VisiteursSvc, PraticiensSvc) {
    var self = this;
    self.praticiens = PraticiensSvc.praticiens;
    self.commandes = [];

    for (var i = 0; i < self.praticiens.length; i++) {
        for (var j = 0; j < self.praticiens[i].Commandes.length; j++) {

            self.praticiens[i].Commandes[j].Praticien = {
                Nom: self.praticiens[i].Nom,
                Prenom: self.praticiens[i].Prenom
            };

            self.commandes.push(self.praticiens[i].Commandes[j]);
        }
    }

    self.get = function (commandeId) {
        for (var i = 0; i < self.commandes.length; i++) {
            if (self.commandes[i].Id == parseInt(commandeId)) {
                self.commandes[i].Visiteur = VisiteursSvc.get(self.commandes[i].IdVisiteur);
                self.commandes[i].Praticien = PraticiensSvc.get(self.commandes[i].IdClient);
                return self.commandes[i];
            }
        }

        return null;
    };

    self.insert = function (commande) {
        $http.put("http://" + UsersSvc.settings.server + "/api/commande.php", commande).success(function (response) {
            if (!response)
                alert("Erreur lors de l'envoi de la commande.");


            response.Praticien = {
                Nom: commande.Praticien.Nom,
                Prenom: commande.Praticien.Prenom
            };

            var praticien = PraticiensSvc.get(commande.Praticien.Id);
            praticien.Commandes.push(response);

            self.commandes.push(response);
        });
    }
})


/*
==================================================================================================
                            SERVICES ~ Définition du service PraticiensSvc

-- Ce service gère toutes les données relatives aux praticiens (infos, commandes...)
==================================================================================================
*/

.service("PraticiensSvc", function ($http, UsersSvc) {
    var self = this;
    self.praticiens = [];

    self.loadPraticiens = $http.get("http://" + UsersSvc.settings.server + "/api/praticien").success(function (response) {
        self.praticiens = response;
    });

    self.get = function (praticienId) {
        for (var i = 0; i < self.praticiens.length; i++) {
            if (self.praticiens[i].Id == parseInt(praticienId))
                return self.praticiens[i];
        }

        return null;
    };
});
