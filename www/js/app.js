﻿/* ==================================================================================================
                APPLICATION ~ Initialisation de l'application GalaxyMedoc

-- On inclut la librairie Ionic et les services utiles à l'application
================================================================================================== */

angular.module("GalaxyMedoc", ["ionic", "GalaxyMedoc.services"])


/* ==================================================================================================
                APPLICATION ~ Configration au démarrage de l'application

-- Code généré par Ionic, ne pas toucher
================================================================================================== */

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            if (cordova.plugins.Keyboard.hideKeyboardAccessoryBar)
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

            cordova.plugins.Keyboard.disableScroll(true);
        }

        if (window.StatusBar)
            StatusBar.styleDefault();
    });
})


/* ==================================================================================================
                APPLICATION ~ Définition des URLs des pages de l'application

-- Définition des pages de l'application : url, template (code html), controller (code js)
================================================================================================== */

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        /* ===========================================================
                 ROUTER ~ Écran de récupération de mot de passe
        =========================================================== */
        .state("loading", {
            url: "/",
            templateUrl: "templates/loading.html",
            controller: "LoadingCtrl",
            controllerAs: "loading",
            controller: function ($state, $scope, $ionicPopup, UsersSvc) {
                var self = this;
                $scope.data = { server: "gsb.konradkevin.com" };

                var alertPopup = $ionicPopup.show({
                    template: "<select ng-model='data.server'><option value='gsb.konradkevin.com'>gsb.konradkevin.com (OVH)</option><option value='192.168.10.12'>192.168.10.12 (VM)</option></select>",
                    title: "Configuration réseau",
                    subTitle: "Choisir un serveur sur lequel se connecter",
                    scope: $scope,
                    buttons: [
                        {
                            text: "Connexion",
                            type: "button-positive",
                            onTap: function (e) { return $scope.data.server; }
                        }
                    ]
                });

                alertPopup.then(function (res) {
                    if (UsersSvc.settings == null)
                        UsersSvc.settings = { server: res, quantiteAuto: true };
                    else
                        UsersSvc.settings.server = res;

                    $state.go("login");
                });
            }
        })


        /* ====================================
                 ROUTER ~ Écran de login
        ==================================== */
        .state("login", {
            url: "/login",
            templateUrl: "templates/login.html",
            controllerAs: "login",
            resolve: {
                visiteurs: function (VisiteursSvc) { return VisiteursSvc.loadVisiteurs; },
                praticiens: function (PraticiensSvc) { return PraticiensSvc.loadPraticiens; },
                medicaments: function (MedicamentsSvc) { return MedicamentsSvc.loadMedicaments; }
            },
            controller: function ($http, $state, $ionicPopup, UsersSvc, VisiteursSvc) {
                var self = this;
                self.visiteurs = VisiteursSvc.visiteurs;
                self.loading = false;
                self.remember = true;

                if (UsersSvc.user != null) {
                    self.user = VisiteursSvc.get(UsersSvc.user.Id);
                    self.user.Password = UsersSvc.user.Password;
                    $state.go("tab.home");
                } else
                    self.user = VisiteursSvc.visiteurs[0];

                self.login = function () {
                    self.loading = true;

                    $http.post("http://" + UsersSvc.settings.server + "/api/user?service=connexion", self.user).success(function (response) {
                        self.loading = false;

                        if (!response) {
                            var alertPopup = $ionicPopup.alert({
                                title: "Connexion",
                                template: "Les identifiants entrés sont incorrects."
                            });

                            return;
                        }

                        if (self.remember)
                            window.localStorage.setItem("user", JSON.stringify(self.user));

                        UsersSvc.user = self.user;
                        $state.go("tab.home");
                    });
                };

                self.showRecover = function () {
                    $state.go("recover", { "visiteurId": self.user.Id });
                }
            }
        })

        /* ===========================================================
                 ROUTER ~ Écran de récupération de mot de passe
        =========================================================== */
        .state("recover", {
            url: "/recover/:visiteurId",
            templateUrl: "templates/recover.html",
            controller: "RecoverCtrl",
            controllerAs: "recover",
            controller: function ($http, $state, $ionicPopup, VisiteursSvc, $stateParams, UsersSvc) {
                var self = this;
                self.loading = false;
                self.user = VisiteursSvc.get($stateParams.visiteurId);

                self.recover = function () {
                    self.loading = true;

                    $http.post("http://" + UsersSvc.settings.server + "/api/user?service=recover", self.user).success(function (response) {
                        var message = "Une erreur est survenue lors de la récupération du mot de passe.";
                        self.loading = false;

                        if (response)
                            message = "Votre nouveau mot de passe a été envoyé à l'adresse <b>" + self.user.Email + "</b>.";

                        var alertPopup = $ionicPopup.alert({
                            title: "Récupération de mot de passe",
                            template: message
                        });

                        alertPopup.then(function (res) {
                            if (res)
                                $state.go("login");
                        });
                    });
                };
            }
        })

        .state("tab", {
            url: "/tab",
            abstract: true,
            templateUrl: "templates/tabs.html"
        })

        /* ========================================
                ROUTER ~ Onglet principal
        ======================================== */
        .state("tab.home", {
            url: "/home",
            views: {
                "tab-home": {
                    templateUrl: "templates/tab-home.html",
                    controllerAs: "home",
                    controller: function (CommandesSvc) {
                        var self = this;
                        self.commandes = CommandesSvc;
                    }
                }
            }
        })

        /* ========================================
                ROUTER ~ Onglet praticiens
        ======================================== */
        .state("tab.praticiens", {
            url: "/praticiens",
            views: {
                "tab-praticiens": {
                    templateUrl: "templates/tab-praticiens.html",
                    controllerAs: "praticiens",
                    controller: function ($state, PraticiensSvc, UsersSvc) {
                        var self = this;
                        self.listCanSwipe = true;
                        self.user = UsersSvc.user;
                        self.settings = UsersSvc.settings;

                        self.praticiens = PraticiensSvc.praticiens.filter(function (item) {
                            for (var i = 0; i < self.user.Secteurs.length; i++) {
                                if (self.user.Secteurs[i].Id == item.Secteur.Id)
                                    return true;
                            }

                            return false;
                        });

                        self.showDetails = function (praticien) {
                            $state.go("tab.praticien-details", { "praticienId": praticien.Id });
                        };

                        self.nouvelleCommande = function (praticien) {
                            $state.go("tab.praticien-commande", { "praticienId": praticien.Id });
                        }
                    }
                }
            }
        })

        /* ==============================================
                ROUTER ~ Onglet commande praticien
        ============================================== */
        .state("tab.praticien-commande", {
            url: "/commande/:praticienId",
            views: {
                "tab-praticiens": {
                    templateUrl: "templates/praticien-commande.html",
                    controllerAs: "commandes",
                    controller: function ($state, $stateParams, PraticiensSvc, MedicamentsSvc, UsersSvc, CommandesSvc, $ionicPopup) {
                        var self = this;
                        self.praticien = PraticiensSvc.get($stateParams.praticienId);
                        self.medicaments = MedicamentsSvc.medicaments;

                        var canvas = document.getElementById('signatureCanvas');
                        self.signaturePad = new SignaturePad(canvas);

                        self.commande = {
                            Visiteur: UsersSvc.user,
                            Praticien: self.praticien,
                            Details: []
                        };

                        // On récupère les médocs de la dernière commande si elle existe et que le param auto est activé
                        if (UsersSvc.settings.quantiteAuto && self.praticien.Commandes.length > 0)
                            self.commande.Details = self.praticien.Commandes[0].Details;

                        self.ajouteMedicament = function () {
                            self.commande.Details.push({ Medicament: self.selectMedoc, Quantite: 1 });
                            self.selectMedoc = null;

                            // On réinitialise la signature car commande changée, donc praticien doit resigner.
                            self.signaturePad.clear();
                        }

                        self.updateQuantite = function (detail, quantite) {
                            var nouvelleValeur = parseInt(detail.Quantite) + quantite;
                            if (nouvelleValeur >= 1 && nouvelleValeur <= 100)
                                detail.Quantite = nouvelleValeur;
                            else if (nouvelleValeur == 0) {
                                self.commande.Details.splice(self.commande.Details.indexOf(detail), 1);
                            }

                            // On réinitialise la signature car commande changée, donc praticien doit resigner.
                            self.signaturePad.clear();
                        }

                        self.validerCommande = function () {
                            if (self.signaturePad.isEmpty()) {
                                var alertPopup = $ionicPopup.alert({
                                    title: "Signature manquante",
                                    template: "Le praticien doit apposer sa signature afin de valider la commande."
                                });
                            }
                            else if (!self.commande.Details.length) {
                                var alertPopup = $ionicPopup.alert({
                                    title: "Commande vide",
                                    template: "Veuillez sélectionner au moins un médicament."
                                });
                            }
                            else {
                                self.commande.Signature = self.signaturePad.toDataURL();
                                CommandesSvc.insert(self.commande);
                                $state.go("tab.praticiens");
                            }
                        }

                        self.medocsAjoutables = function (medoc) {
                            return self.commande.Details.filter(function (item) {
                                return item.Medicament.Id == medoc.Id
                            }).length == 0;
                        };
                    }
                }
            }
        })

        /* ==============================================
                ROUTER ~ Onglet détails commandes
        ============================================== */
        .state("tab.commande-details", {
            url: "/commande-details/:commandeId",
            views: {
                "tab-praticiens": {
                    templateUrl: "templates/commande-details.html",
                    controllerAs: "commande",
                    controller: function ($stateParams, CommandesSvc) {
                        var self = this;
                        self.commande = CommandesSvc.get($stateParams.commandeId);
                    }
                }
            }
        })
        /* ==============================================
                ROUTER ~ Onglet détails praticien
        ============================================== */
        .state("tab.praticien-details", {
            url: "/praticiens/:praticienId",
            views: {
                "tab-praticiens": {
                    templateUrl: "templates/praticien-details.html",
                    controllerAs: "praticien",
                    controller: function ($stateParams, $state, PraticiensSvc) {
                        var self = this;
                        self.praticien = PraticiensSvc.get($stateParams.praticienId);
                        self.showCommande = function (praticien) {
                            $state.go("tab.praticien-commande", { "praticienId": praticien.Id });
                        };
                    }
                }
            }
        })

        /* ==============================================
                ROUTER ~ Onglet compte/paramètres
        ============================================== */
        .state("tab.account", {
            url: "/account",
            views: {
                "tab-account": {
                    templateUrl: "templates/tab-account.html",
                    controllerAs: "account",
                    controller: function ($state, $ionicPopup, UsersSvc) {
                        var self = this;
                        self.settings = UsersSvc.settings;

                        self.save = function () {
                            window.localStorage.setItem("settings", JSON.stringify(UsersSvc.settings));
                        };

                        self.logout = function () {
                            UsersSvc.updateUser(null, false);
                            $state.go("login");
                        };

                        self.clearCache = function () {
                            window.localStorage.clear();

                            var alertPopup = $ionicPopup.alert({
                                title: "Vider le cache",
                                template: "Toutes les préférences et données utilisateur de l'application ont été supprimées"
                            });
                        };
                    }
                }
            }
        });

    $urlRouterProvider.otherwise("/");
});
